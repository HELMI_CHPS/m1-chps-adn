#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include "projet.h"

int main() {
  //Encodage de l'ADN et Création de l'ARN
  FILE* fichier = NULL;
  FILE* binaire = NULL;
  
  system("cp adn arn");
  system("rm binaire");
  system("touch binaire");
  
  system("rm Zones_a_risques");
  system("touch Zones_a_risques");

  fichier = fopen("arn", "r+");
  binaire = fopen("binaire", "r+");
  
  int nb_lettres = encode(&fichier, &binaire);
  
  fclose(fichier);
  fclose(binaire);
  //////////////////////////////////////////////
  
  binaire = fopen("binaire", "r+");	
  
  //Recherche codon

  char quatre_lettres = fgetc(binaire);
  int t_adn=nb_lettres-(nb_lettres%3);
  unsigned char adn[t_adn];
  int quot=4, conv2=(int)(quatre_lettres);

  for(int i=0;i<t_adn;i++) {      
    adn[i]=quatre_lettres & 3;
    conv2>>=2;
    quot=quot-1;
    quatre_lettres>>=2;
    i++;
    adn[i]=quatre_lettres & 3;
    conv2>>=2;
    quot=quot-1;
    quatre_lettres>>=2;
    i++;
    adn[i]=quatre_lettres & 3;
    conv2>>=2;
    quot=quot-1;
    quatre_lettres>>=2;
    i++;
    adn[i]=quatre_lettres & 3;
    conv2>>=2;
    quot=quot-1;
    quatre_lettres>>=2;
    
    quatre_lettres = fgetc(binaire);
	}
  
	system("rm chaineProt; touch chaineProt");
	int StartStop1[T_StartStop][2]={0};
  int StartStop2[T_StartStop][2]={0}; 
  
  //remplit les tableaux avec les start/stop, puis fait la traduction et la recherche des risques de mutation
  chercheZonesCodantes(t_adn, adn, StartStop1, StartStop2);

    
// Detection des zones d'alignement
  system("rm ResAlignement; touch ResAlignement");
  FILE* fichierAlignement = NULL;
  FILE* ADNaComparer = NULL;
	ADNaComparer = fopen("adn2", "r"); 
  fichierAlignement = fopen("ResAlignement", "a+");      
	alignement(t_adn, adn, ADNaComparer, &fichierAlignement);
  fclose(ADNaComparer);
  fclose(fichierAlignement);
  
  //~ system("rm arn_trad_prot; touch arn_trad_prot");
  //~ FILE* arnTrad = NULL;
  //~ FILE* protPourTrad = NULL;
	//~ arnTrad = fopen("arn_trad_prot", "a+"); 
  //~ printf("conversion\n");
  //~ protPourTrad = fopen("proteineEnArn", "r");      
	//~ tradProtArn(&arnTrad, protPourTrad);
  //~ fclose(arnTrad);
  //~ fclose(protPourTrad);
  
  return 0;
}

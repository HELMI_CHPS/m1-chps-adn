#ifndef _projet_h_
#define _projet_h_
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

#define T_StartStop 1000

char tradCodon[22][5]={ "Stop", "Lys", "Asn","Thr","Arg","Ser", "Ile","Met",
		"Gln","His","Pro","Leu",
		"Glu","Asp","Ala","Gly","Val",
		"Tyr","Ser","Cys","Trp","Phe"};
int codon[4][4][4]={ { {1,2,1,2} , {3,3,3,3} , {4,5,4,5} , {6,6,7,6} },
					  { {8,9,8,9} , {10,10,10,10} , {4,4,4,4} , {11,11,11,11} },
					  { {12,13,12,13} , {14,14,14,14} , {15,15,15,15} , {16,16,16,16} },
					  { {0,17,0,17} , {18,18,18,18} , {0,19,20,19} , {11,21,11,21} }};


int puiss(unsigned char n) {
	int a=1;
	if (n==0) return 1;
	for(int i=0; i<n;i++) {a=a*2;}
	return a;
}

void convertirEnARN (FILE* fichier) {          
  fseek(fichier, -1, SEEK_CUR);
  fputc('U', fichier);	
}

int encode(FILE** fichier, FILE** binaire) {
  int caractereActuel = 0;
  int compteur=0;
  int conv=0;
  int taille_binaire=0;
  int nb_lettres=0;

  //~ encodage(&fichier, &binaire
	//Création du binaire
  while (1) {     
    caractereActuel = fgetc(*fichier);
    while((caractereActuel == '\n') || (caractereActuel == ' ')) 
      caractereActuel = fgetc(*fichier);
    if((caractereActuel != EOF) && (caractereActuel != 10)) {
      compteur=2*((ftell(*fichier)-1) & 3);
      
      nb_lettres++;
      
      switch (caractereActuel){
      case 'a':
				break;
      case 'A':
				break;
			case 'c':
				conv=conv+puiss(compteur);
        break;
			case 'C':
				conv=conv+puiss(compteur);
        break;
			case 'g':
				conv=conv+puiss(compteur+1);
        break;
			case 'G':
				conv=conv+puiss(compteur+1);
        break;
			case 't':
				conv=conv+puiss(compteur);
				conv=conv+puiss(compteur+1);
        convertirEnARN(*fichier);
        break;
			case 'T':
				conv=conv+puiss(compteur);
				conv=conv+puiss(compteur+1);
        convertirEnARN(*fichier);
        break;
			}
      if(((ftell(*fichier)-1) & 3) && (ftell(*fichier)>1)) {
        fseek(*binaire, -1, SEEK_CUR);
        fputc(conv, *binaire);
      }
      else if(ftell(*fichier)==1) {
        fputc(conv, *binaire);
        taille_binaire++;
      }
      else {
        fputc(conv, *binaire);
        taille_binaire++;
      }
      
      if(compteur==6) {
        conv=0;
      }
    }
    else {
      break;
    }
    
  } 
  return nb_lettres;
}

int look_up(int debut, int val_codon, int t_adn, unsigned char* adn) {
  for(int i=debut;i<t_adn;i+=3) {
    if(codon[adn[i]][adn[i+1]][adn[i+2]] ==  val_codon) {
      return i;
    }
  }
  return -1;
}

void initStartStop(int StartStop[][2]) {
  for(int i=0;i<T_StartStop;i++) {
    StartStop[i][1]=0;
    StartStop[i][0]=0;
  }
}

void chercheMutation(unsigned char* adn, int start, int stop, FILE** fichierMutation) {
  int i=start-1;
  float tmp=0, k=0;
  while(i<(stop)) {
    if((adn[i] == 1) || (adn[i]==2)) {
      tmp=0;
      for(int j=i+1;j<=stop;j++) {
        if((adn[j] != 1) && (adn[j]!=2)) {
          i=j;
          k+=(tmp/(stop-start));
          break;
        }
        else {
          tmp++;
        }
      }
    }
  i++;
  }
  fprintf(*fichierMutation, "%d%%", (int)(k*100));
  fprintf(*fichierMutation, "\n");
}

void lettreEnChiffre(int* x) {
  switch (*x){
    case 'A':
      *x=0;
      break;
    case 'C':
      *x=1;
      break;
    case 'G':
      *x=2;
      break;
    case 'T':
      *x=3;
      break;
  }
}

void alignement(int t_adn, unsigned char*adn, FILE* ADNaComparer, FILE** fichierAlignement) {
  int alig=0;
  int a=fgetc(ADNaComparer);
  int b=fgetc(ADNaComparer);
  int c=fgetc(ADNaComparer);
  while ((a != EOF)  && (b != EOF) && (c != EOF)) {
       alig=0;
       fprintf(* fichierAlignement, "%c",a);
       fprintf(* fichierAlignement, "%c",b);
       fprintf(* fichierAlignement, "%c : ",c);
       lettreEnChiffre(&a);
       lettreEnChiffre(&b);
       lettreEnChiffre(&c);
       for (int i=0; i<t_adn; i+=3) {
         if (adn[i]==a) {
           if (adn[i+1]==b) {
             if (adn[i+2]==c) {
               alig++;
             }
           }
         }
       }
       fprintf(* fichierAlignement, "%d\n", alig);
       a=fgetc(ADNaComparer);
       b=fgetc(ADNaComparer);
       c=fgetc(ADNaComparer);
  }
}

void traduire(unsigned char* adn, int start, int stop, FILE** chaineCodante){
  for(int j=start;j<stop;j=j+3) {
    fprintf(*chaineCodante, "%s ", tradCodon[codon[adn[j]][adn[j+1]][adn[j+2]]]);
  }
  fprintf(*chaineCodante, "\n");
}

void parcoursStartStop(unsigned char* adn, int StartStop[][2], int* nbChaineGeneral) {
  FILE* chaineCodante = NULL;
	chaineCodante = fopen("chaineProt", "a+");
  FILE* fichierMutation = NULL;
	fichierMutation = fopen("Zones_a_risques", "a+");
    
  for(int i=0;i<T_StartStop;i++) {
    if (StartStop[i][1]==0) {
      initStartStop(StartStop);
      break;
    }
    fprintf(chaineCodante, "%d: ", *nbChaineGeneral);
    fprintf(fichierMutation, "%d: ", *nbChaineGeneral);
    traduire(adn, (StartStop[i][0]+3), StartStop[i][1], &chaineCodante);
    chercheMutation(adn, (StartStop[i][0]+3), StartStop[i][1], &fichierMutation);
    (*nbChaineGeneral)++;
  }
  fclose(chaineCodante);
  fclose(fichierMutation);
}

void stockStartStop(int numero, int start, int stop, int StartStop[][2]) {
  StartStop[numero][0]= start;
  StartStop[numero][1]= stop;
}

void chercheZonesCodantes(int fin, unsigned char* adn, int StartStop1[][2], int StartStop2[][2]) {
  int cherche=1, cible=1;
	int debut=0, compteurZoneCodante=0, nbChaineGeneral=0, start=-1, stop=0, etat=0; //etat: 0 -> cherche start ___ : 1 ->cherche stop
  fin--;
	
	while(cherche==1) {
		start=look_up(stop, 7, fin, adn);      
    if ( start >= 0 ) {
      stop=look_up(start+3, 0, fin, adn);
      if ( stop > start+3 ) {        
        if(cible==1) { //écrit start et stop dans le tableau StartStop1
          stockStartStop(compteurZoneCodante, start, stop, StartStop1);
          if(compteurZoneCodante==T_StartStop-1) {
            cible*=-1;
            parcoursStartStop(adn, StartStop1, &nbChaineGeneral);
            compteurZoneCodante=-1;
          }
        }
        else { //écrit start et stop dans le tableau StartStop2
          stockStartStop(compteurZoneCodante, start, stop, StartStop2);
          if(compteurZoneCodante==T_StartStop-1) {
            cible*=-1;
            parcoursStartStop(adn, StartStop2, &nbChaineGeneral);
            
            compteurZoneCodante=-1;
          }
        }
        compteurZoneCodante++;
      }
    } 
    else {
      if(cible==1) {
        parcoursStartStop(adn, StartStop1, &nbChaineGeneral);
        
      }
      else {
        parcoursStartStop(adn, StartStop2, &nbChaineGeneral);
      }
      cherche=15;
    }
  }
}
#endif

